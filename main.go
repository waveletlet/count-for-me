package main

import (
	"fmt"
	"time"

	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/gl"

	"gitlab.com/waveletlet/count-for-me/counters"
	"gitlab.com/waveletlet/count-for-me/sound"

	"github.com/dskinner/material"
)

var (
	env = new(material.Environment)

	button      *material.Button
	floatButton *material.FloatingActionButton
	toolbar     *material.Toolbar
	menu        *material.Menu
)

func main() {
	fmt.Println("Running main()")
	tone0 := sound.Tone{116.35, (70 * time.Millisecond)}
	counter := counters.New(10)
	// TODO adding intervals, setting sounds should be done in the UI
	tone1 := sound.Tone{230.8, (70 * time.Millisecond)}
	counter.AddIntervalSet(1, 4, tone1)
	tone2 := sound.Tone{530.8, (70 * time.Millisecond)}
	counter.AddIntervalSet(5, 2, tone2)

	app.Main(func(a app.App) {
		var glctx gl.Context
		sz := size.Event{}

		a.RegisterFilter(counters.TickFilter)
		for e := range a.Events() {
			// Filter received events
			switch e := a.Filter(e).(type) {
			case lifecycle.Event:
				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					glctx, _ = e.DrawContext.(gl.Context)
					onStart(glctx)        // demo material
					a.Send(paint.Event{}) // demo material
				case lifecycle.CrossOff:
					//do a thing on lose focus
					env.Unload(glctx) // demo material
					glctx = nil       // demo material
				}
			case size.Event:
				sz = e
				// demo material
				fmt.Printf("sz = %v\n", sz)
				if glctx == nil {
					a.Send(e) // republish event until onStart is called
				} else {
					onLayout(e)
				}
			case paint.Event:
				if glctx == nil || e.External {
					continue
				}
				onPaint(glctx) // demo material
				//drawTick(glctx, sz)
				a.Publish()
				a.Send(paint.Event{}) // keep animating
			case touch.Event:
				env.Touch(e) // demo material
				if down := e.Type == touch.TypeBegin; down || e.Type == touch.TypeEnd {
					fmt.Printf("counter.Run() tapped at %v\n", time.Now())
					go counter.Run()
					flashRed(glctx, sz)
					a.Publish()
					if err := tone0.Play(); err != nil {
						fmt.Println(err)
					}
				}
			case counters.Event:
				fmt.Printf("Ticking at: %v\n", time.Now())
				flashBlue(glctx, sz)
				e.Tone.Play()
				a.Publish()
			}

			select {
			case e := <-counter.Events():
				a.Send(e)
			default:
			}
		}
	})
}

// color flashy stuff for debugging
var flashColor = []float32{0, 1, 1}

func flashRed(glctx gl.Context, sz size.Event) {
	glctx.ClearColor(1, 0, 0, 1)
	glctx.Clear(gl.COLOR_BUFFER_BIT)
}

func flashGreen(glctx gl.Context, sz size.Event) {
	glctx.ClearColor(0, 1, 0, 1)
	glctx.Clear(gl.COLOR_BUFFER_BIT)
}

func flashBlue(glctx gl.Context, sz size.Event) {
	glctx.ClearColor(0, 0, 1, 1)
	glctx.Clear(gl.COLOR_BUFFER_BIT)
}

func drawTick(glctx gl.Context, sz size.Event) {
	glctx.ClearColor(flashColor[0], flashColor[1], flashColor[2], 1)
	glctx.Clear(gl.COLOR_BUFFER_BIT)
}

func onStart(ctx gl.Context) {
	env.SetPalette(material.Palette{
		Primary: material.Color(0x0D47A1),
		Dark:    material.Color(0x002171),
		Light:   material.Color(0x5472D3),
		Accent:  material.Color(0x00ACC1),
	})

	ctx.Enable(gl.BLEND)
	ctx.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	ctx.Enable(gl.CULL_FACE)
	ctx.CullFace(gl.BACK)

	env.Load(ctx)
	env.LoadGlyphs(ctx)

	button = env.NewButton(ctx)
	button.SetTextColor(material.White)
	button.SetText("AAAH`e_llo |jJ go 112px")
	button.BehaviorFlags = material.DescriptorFlat

	floatButton = env.NewFloatingActionButton(ctx)
	floatButton.SetTextColor(material.White)
	floatButton.SetText("Hello go 56px")
	floatButton.BehaviorFlags = material.DescriptorFlat

	toolbar = env.NewToolbar(ctx)
	toolbar.SetTextColor(material.White)
	toolbar.SetText("Hello go 45px")
	toolbar.BehaviorFlags = material.DescriptorFlat
	toolbar.AddAction(button)

	menu = env.NewMenu(ctx)
	menu.SetTextColor(material.White)
	menu.SetText("Hello go 34px")
	menu.BehaviorFlags = material.DescriptorFlat
	menu.AddAction(button)
}

func onLayout(sz size.Event) {
	env.SetOrtho(sz)
	fmt.Printf("env.Grid: %v\n", env.Grid)
	fmt.Printf("env.Grid.Margin: %v\n", env.Grid.Margin)
	fmt.Printf("env.Grid.Gutter: %v\n", env.Grid.Gutter)
	fmt.Printf("env.Grid.Columns: %v\n", env.Grid.Columns)
	env.StartLayout()
	env.AddConstraints(
		toolbar.Width(1290), toolbar.Height(45), toolbar.Z(1), toolbar.StartIn(env.Box, env.Grid.Gutter),
		toolbar.TopIn(env.Box, env.Grid.Gutter),
		button.Width(1290), button.Height(112), button.Z(1), button.StartIn(env.Box, env.Grid.Gutter),
		button.Below(floatButton.Box, env.Grid.Gutter),
		floatButton.Width(620), floatButton.Height(56), floatButton.Z(1), floatButton.StartIn(env.Box, env.Grid.Gutter),
		floatButton.Below(button.Box, env.Grid.Gutter),
		menu.Width(1290), menu.Height(300), menu.Z(1), menu.StartIn(env.Box, env.Grid.Gutter),
		menu.Below(floatButton.Box, env.Grid.Gutter),
	)
	fmt.Println("starting layout")
	t := time.Now()
	env.FinishLayout()
	fmt.Printf("finished layout in %s\n", time.Now().Sub(t))
}

var lastpaint time.Time
var fps int

func onPaint(ctx gl.Context) {
	ctx.ClearColor(material.BlueGrey500.RGBA())
	ctx.Clear(gl.COLOR_BUFFER_BIT)
	env.Draw(ctx)
	now := time.Now()
	fps = int(time.Second / now.Sub(lastpaint))
	lastpaint = now
}
