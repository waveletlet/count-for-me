# Notes
I'm learning go! Next steps, key concepts to keep in mind, lessons learned...


## Status
Next up: basic UI 

Button/Slider UI idea:
- Main screen initial view:
  - "Edit Timer" page
- EditTimer page:
  - Slider for total time, with box displaying current value and button to "Set
    time"
  - Intervals section
    - Display default interval
      - Edit / delete buttons
    - "+" button to add interval
  - "Start" button
    - Goes to Running Timer page
- New/edit Interval page:
  - Slider for interval size
    - Scale based on total time (=max)
    - Box for current value
    - Round to increments of 5 sec if timer > 60 s
    - "Set" button
  - Slider for tone frequency
    - Box for current value
    - Range based on human hearing, probably cut off lower/upper end of range
    - "Preview" buttons
    - Slider for duration?
  - "Set" or "Save"
    - Returns to Edit Timer page
- Running Timer page:
  - Display current time (out of total)
  - Display counter for intervals?



Trying to use https://github.com/dskinner/material for making an UI.
Working through the repo making example programs to figure out what's going on.
Discovered the x,y starts in a different corner than expected:
- touch.Event 0,0 upper left corner
- material.Material.World() matrix 0,0 lower left corner

Quick color schemeing:
https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=0D47A1&secondary.color=00ACC1&secondary.text.color=000000

Material standard Z heights
https://storage.googleapis.com/spec-host-backup/mio-design%2Fassets%2F0B6xUSjjSulxceF9udnA4Sk5tdU0%2Fbaselineelevation-chart.png

### Tangent: Flutter
Didn't really want to try to learn a second language at the same time as go, but
it looks like it's easier to make a UI with flutter.

Resources:
https://flutter.dev/docs/get-started/test-drive?tab=terminal#terminal
https://sites.google.com/a/athaydes.com/renato-athaydes/posts/buildingamobilefrontendforagoapplicationusingflutter

Installed all deps except Android Studio, trying to see how far I can get
without it.


Generate java bindings:
`gomobile bind -target=android/arm gitlab.com/waveletlet/count-for-me/counters`

Output had lots of commented messages "// skipped field ....." because they contained types
that don't work in java(?)

Guide only has instructions on including the exported bindings as a dependency
for Android Studio:
https://developer.android.com/studio/projects/android-library#AddDependency

But does adding the package details to a build.gradle and a settings.gradle do
the same thing?

Next step I would somehow have to expose the Go api in the java, but now I'm
using 3 languages?!?!? I have the option of Kotlin, too. Maybe there's other
ways to do this....

https://github.com/adieu/flutter_go this example looks more simple, but might
not be suitable for my situation (go part as rpc, basically a local server for
the flutter client)


## TODO
### UI 
Mehr klickybunty!

Libraries that might be helpful:
- https://godoc.org/golang.org/x/mobile/exp/font
- https://github.com/shibukawa/nanovgo vector graphics engine
  - no idea how to implement this in this app, uses a different gl library and
    not sure how i'd go about swapping it out, they have totally different apis
    it seems. also it didn't build on my compy (probably just need to dl some
    libraries though)
Example gomobile apps:
- https://github.com/dskinner/material No longer developed but maybe good
  example; has text rendering
- https://github.com/antoine-richard/gomobile-text Has a basic example of
  rendering text 
- https://github.com/hajimehoshi/ebiten/ A whole game engine that's probably
  more than I need
- https://github.com/EngoEngine/engo another game engine, with better gomobile
  examples maybe (doesnt actually build though)
- https://github.com/ianremmler/trigo Simple card game (like "Set")
- https://github.com/dr4ke616/gospeedtest probably renders text but I didn't
  feel like debugging it so it builds (built using a pretty old version of
  gomobile so the api's changed since then)


Or maybe I should just figure out Qt
https://github.com/therecipe/qt


### Audio
how do I .wav?

Libraries that might be helpful:
- https://godoc.org/golang.org/x/mobile/asset

## Other problems / Questions to return to
- IntervalSet tick priority ranking: Currently the longer intervals have the
  highest priority, but maybe this should be revisited.

## Breadcrumbs and clues and mysteries......
## Lessons
### Channels
- Don't close a channel that receives from multiple sources unless you
  absolutely know everything is done sending.

### Time
- `time.AfterFunc(d Duration, f func())` variable evaluation:
  - A variable passed through f will have the value of the variable at the time of
    timer expiration, not when the AfterFunc timer was started.
  - The following code didn't run as I expected, it kept sending the first tick
    over and over. (I'm still not sure why it wasn't sending the last one
    repeatedly, since that was the last value of tk.)

```
func (self *Counter) sendTicks() {
	fmt.Println("Sending ticks...")
	for _, tk := range self.ticks {
		fmt.Printf("sendTicks() sending %v at %v\n", tk, tk.fireAfter)
		time.AfterFunc(tk.fireAfter, func() { self.sendTick(tk) })
	}
}

func (self *Counter) sendTick(tk Tick) {
	fmt.Printf("counter.sendTick sending %v to evChan\n", tk)
	self.evChan <- Event{tk}
}
```

Logs looked like:

```
04-01 19:42:39.453  3645  3661 I GoLog   : Sending ticks...
04-01 19:42:39.453  3645  3661 I GoLog   : sendTicks() sending {1000000000 {1 4 false}} at 1s
04-01 19:42:39.453  3645  3661 I GoLog   : sendTicks() sending {2000000000 {1 1 false}} at 2s
04-01 19:42:40.453  3645  3662 I GoLog   : counter.sendTick sending {10000000000 {1 1 false}} to evChan
04-01 19:42:40.483  3645  3662 I GoLog   : Ticking at: 2019-04-01 19:42:40.494417719 -0300 UYT m=+2.224732917
04-01 19:42:41.453  3645  3662 I GoLog   : counter.sendTick sending {10000000000 {1 1 false}} to evChan

```

So I had to wrap the anonymous func in another func where the variable is
evaluated on timer start. Or I think that's what's going on? It's kind of black
magic...

```
		time.AfterFunc(tk.fireAfter, func(t Tick) func() {
			return func() { self.sendTick(t) }
		}(tk))

```

Logs:

```
04-01 19:54:21.763  4234  4364 I GoLog   : counter.sendTick sending {1000000000 {1 4 false}} to evChan
04-01 19:54:21.793  4234  4251 I GoLog   : Ticking at: 2019-04-01 19:54:21.806020732 -0300 UYT m=+5.489818593
04-01 19:54:22.763  4234  4250 I GoLog   : counter.sendTick sending {2000000000 {1 1 false}} to evChan
```

### Faster builds:
Don't just `gomobile install`, faster if it's not compiling for every possible
target. For android only, `gomobile install -x -target=android/arm`. (`-x` or '-x -v` for detailed output)

## Solved problems:
### Replace Timer with WaitGroup to reset counter.running
time.Timer running at the same time as each tick's time.AfterFunc. This means
the last tick ends up running ca. 100 ms after the timer stops so there's a
fraction of a second where counter.running is "false" when it's actually still
running. A better solution would be setting up a hook to run after the last tick
to reset counter.running.

It's better now with a WaitGroup. Still get the "All done!" message 20 ms or so
before playing the last tone, but at least all of the ticks are sent by the time
the counter.running switch is toggled.

__solved by commit ca14b6daad720ffda6ebca2d3b02ba78ee868d8d__

### Tones for Ticks
Make a tick pass the audio to the app and play unique tones for each IntervalSet.

__solved by commit f9455fa124d01edbc2139d62feda8c12b0e9eb06__

### IntervalSet tick priority
Need to deal with setting priority of what IntervalSet has higher tick priority,
and overwrite lower priority ticks at that timepoint.

__solved by commit 01968a9a5432239b7872d26937bd7e8f75d7d315__

### Merge all ticks into one queue
Tickers aren't all starting at t0. Maybe it would be better to line up all the
ticks at once and set up a tick queue before starting the timer, rather than
having a bunch of goroutines trying to run for each interval set.

__solved by commit abb530dfa4763031a3c7740e167fe4e06ac49d31__

### Filtering tick events
Trying to get events from the counter to the app. The ticker goworkers were each
bringing the first tick to the channel and just waiting there. I guess the way I
was trying to range over the channel in the app code wasn't pulling ticks out of
the queue.

__`app.Filter()`__

- `app.Send(event)` sends an event to the app to be filtered. I can find
  examples of sending (for example, `github.com/golang/mobile/app/shiny.go:43`),
  but can't find an example of registering a filter. Where is the app getting
  it's default filters from??
- `github.com/golang/mobile/app/darwin_desktop.go` has a lot of examples of
  passing events directly into app.eventsIn, but because it's private, I can't
  do it that way. And I still don't see where a filter is getting set.

Code for the relevant functions:

```
type App interface{
  //...
	Send(event interface{})

  // Filter calls each registered event filter function in sequence.
  Filter(event interface{}) interface{}

	// RegisterFilter registers a event filter function to be called by Filter. The
	// function can return a different event, or return nil to consume the event,
	// but the function can also return its argument unchanged, where its purpose
	// is to trigger a side effect rather than modify the event.
  RegisterFilter(f func(interface{}) interface{})
}


func (a *app) Send(event interface{}) {
	a.eventsIn <- event
}

func (a *app) Filter(event interface{}) interface{} {
	for _, f := range a.filters {
		event = f(event)
	}
	return event
}

func (a *app) RegisterFilter(f func(interface{}) interface{}) {
	a.filters = append(a.filters, f)
}


```

__Solved by commit 9702b8ad775ba1d42fb28742e471ae1cf7eae6dc__

