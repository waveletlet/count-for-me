# Count for me

A pure go android interval timer with customisable audio cues. See `notes.md`
for detailed status updates. (Short status update: nothing to see here....)


## Features

(to be implemented)
- Audibly count up or down
- Speak every tick or set intervals
- Ability to set decreasing/increasing intervals
  - i.e. Every 10 sec until last 10 sec, then every sec
- Ability to use TTS or import/record own samples
- Ability to "speak" cues other than numbers
  - ex. "Next", "switch", "rest", "exercise name", "inhale"....
