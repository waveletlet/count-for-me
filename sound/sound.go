package sound

import (
	"io"
	"math"
	"time"

	"github.com/hajimehoshi/oto"
)

var (
	sampleRate      = 44100
	channelNum      = 2
	bitDepthInBytes = 2
	theContext      *oto.Context
)

type sineWave struct {
	freq   float64
	length int64
	pos    int64

	remaining []byte
}

type Tone struct {
	Freq     float64
	Duration time.Duration
}

var DefaultTone = Tone{100, (70 * time.Millisecond)}

func (self Tone) Play() error {
	if theContext == nil {
		var err error
		theContext, err = oto.NewContext(sampleRate, channelNum, bitDepthInBytes, 4096)
		if err != nil {
			return err
		}
	}

	go func() error {
		if err := play(theContext, self.Freq, self.Duration); err != nil {
			return (err)
		}
		return nil
	}()
	return nil
}

func newSineWave(freq float64, duration time.Duration) *sineWave {
	l := int64(channelNum) * int64(bitDepthInBytes) * int64(sampleRate) * int64(duration) / int64(time.Second)
	l = l / 4 * 4
	return &sineWave{
		freq:   freq,
		length: l,
	}
}

func (s *sineWave) Read(buf []byte) (int, error) {
	if len(s.remaining) > 0 {
		n := copy(buf, s.remaining)
		s.remaining = s.remaining[n:]
		return n, nil
	}

	if s.pos == s.length {
		return 0, io.EOF
	}

	eof := false
	if s.pos+int64(len(buf)) > s.length {
		buf = buf[:s.length-s.pos]
		eof = true
	}

	var origBuf []byte
	if len(buf)%4 > 0 {
		origBuf = buf
		buf = make([]byte, len(origBuf)+4-len(origBuf)%4)
	}

	length := float64(sampleRate) / float64(s.freq)

	num := (bitDepthInBytes) * (channelNum)
	p := s.pos / int64(num)
	switch bitDepthInBytes {
	case 1:
		for i := 0; i < len(buf)/num; i++ {
			const max = 127
			b := int(math.Sin(2*math.Pi*float64(p)/length) * 0.3 * max)
			for ch := 0; ch < channelNum; ch++ {
				buf[num*i+ch] = byte(b + 128)
			}
			p++
		}
	case 2:
		for i := 0; i < len(buf)/num; i++ {
			const max = 32767
			b := int16(math.Sin(2*math.Pi*float64(p)/length) * 0.3 * max)
			for ch := 0; ch < channelNum; ch++ {
				buf[num*i+2*ch] = byte(b)
				buf[num*i+1+2*ch] = byte(b >> 8)
			}
			p++
		}
	}

	s.pos += int64(len(buf))

	n := len(buf)
	if origBuf != nil {
		n = copy(origBuf, buf)
		s.remaining = buf[n:]
	}

	if eof {
		return n, io.EOF
	}
	return n, nil
}

func play(context *oto.Context, freq float64, duration time.Duration) error {
	p := context.NewPlayer()
	s := newSineWave(freq, duration)
	if _, err := io.Copy(p, s); err != nil {
		return err
	}
	if err := p.Close(); err != nil {
		return err
	}
	return nil
}

//func Run() error {
//	const (
//		freqC = 523.3
//		freqE = 659.3
//		freqG = 784.0
//	)
//
//	c, err := oto.NewContext(sampleRate, channelNum, bitDepthInBytes, 4096)
//	if err != nil {
//		return err
//	}
//
//	var wg sync.WaitGroup
//
//	wg.Add(1)
//	go func() {
//		defer wg.Done()
//		if err := play(c, freqC, 1*time.Second); err != nil {
//			panic(err)
//		}
//	}()
//
//	wg.Add(1)
//	go func() {
//		defer wg.Done()
//		time.Sleep(1 * time.Second)
//		if err := play(c, freqE, 1*time.Second); err != nil {
//			panic(err)
//		}
//	}()
//
//	wg.Add(1)
//	go func() {
//		defer wg.Done()
//		time.Sleep(2 * time.Second)
//		if err := play(c, freqG, 1*time.Second); err != nil {
//			panic(err)
//		}
//	}()
//
//	wg.Wait()
//	c.Close()
//	return nil
//}
