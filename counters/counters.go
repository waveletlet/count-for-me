package counters

import (
	"fmt"
	"sort"
	"sync"
	"time"

	"gitlab.com/waveletlet/count-for-me/sound"
)

type Counter struct {
	Length       int
	IntervalSets []IntervalSet
	ticks        []Tick
	evChan       chan Event
	running      bool
}

type IntervalSet struct {
	startingTick int
	interval     int
	tone         sound.Tone
}

type Event struct {
	Tone sound.Tone
}

type Tick struct {
	fireAfter   time.Duration
	intervalSet IntervalSet // placeholder to be replaced with relevant attrs based on parent IntervalSet
	tone        sound.Tone
	// the action to be performed at tick timepoint will also go in here
}

func New(length int) *Counter {
	fmt.Println("Making new counter")
	return &Counter{Length: length, IntervalSets: []IntervalSet{DefaultIntervalSet()}, ticks: make([]Tick, length), evChan: make(chan Event)}
}

func DefaultIntervalSet() IntervalSet {
	return IntervalSet{startingTick: 1, interval: 1, tone: sound.DefaultTone}
}

func (self *Counter) AddIntervalSet(startTick, interval int, tone sound.Tone) {
	if startTick < 1 {
		startTick = 1
	}
	self.IntervalSets = append(self.IntervalSets, IntervalSet{startingTick: startTick, interval: interval, tone: tone})
}

func (self *Counter) Run() {
	fmt.Println("Attempting to run counter")
	if !self.running {
		self.running = true
		fmt.Printf("Starting %v second counter\n", self.Length)
		sort.Slice(self.IntervalSets, func(i, j int) bool {
			return self.IntervalSets[i].interval < self.IntervalSets[j].interval
		})
		for _, iv := range self.IntervalSets {
			self.scheduleTicks(iv, self.Length)
		}

		var wg sync.WaitGroup
		wg.Add(self.Length)
		self.sendTicks(&wg)

		wg.Wait()
		fmt.Printf("All done!\n")
		self.running = false
	} else {
		fmt.Println("Counter already running")
	}
}

func TickFilter(ev interface{}) interface{} {
	// Filter to register with app.App and handle tick events
	switch ev.(type) {
	case Event:
		return ev
	default:
		return ev
	}
}

func (self *Counter) Events() <-chan Event {
	return self.evChan
}

func (self *Counter) scheduleTicks(iv IntervalSet, tLen int) {
	fmt.Printf("Scheduling ticks from %v\n", iv)
	for i := iv.startingTick; i <= tLen; i += iv.interval {
		self.ticks[i-1] = Tick{fireAfter: time.Duration(i) * time.Second, intervalSet: iv, tone: iv.tone}
	}
}

func (self *Counter) sendTicks(wg *sync.WaitGroup) {
	fmt.Println("Sending ticks...")
	for _, tk := range self.ticks {
		fmt.Printf("sendTicks() sending %v at %v\n", tk, tk.fireAfter)
		time.AfterFunc(tk.fireAfter, func(t Tick) func() {
			return func() { self.sendTick(t, wg) }
		}(tk))
	}
}

func (self *Counter) sendTick(tk Tick, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Printf("counter.sendTick sending %v to evChan\n", tk)
	self.evChan <- Event{tk.tone}
}
